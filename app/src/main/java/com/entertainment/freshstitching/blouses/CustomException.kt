package com.entertainment.freshstitching.blouses

class CustomException(message: String) : Exception(message)
